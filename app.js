var createError = require('http-errors');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
const jwtMiddleware = require('express-jwt');

const locales = require('./routes/locales');
const users = require('./routes/users');
const sessions = require('./routes/sessions');
const favorites = require('./routes/favorites');
const favoritePlaces = require('./routes/favoritePlaces');
const visits = require('./routes/visits');
const visitsPlaces = require('./routes/visitsPlaces');
const applications = require('./routes/applications');

const findAppBySecret = require('./middleware/findAppBySecret');
const findAppByApplicationId = require('./middleware/findAppByApplicationId');
const authApp = require('./middleware/authApp')();
const allowCORs = require('./middleware/allowCORs')();

const db = require('./config/database');
const secrets = require('./config/secrets');

const localesPath = '/negocios';
const usersPath = '/usuarios';
const sessionsPath = '/sesion';
const favoritesPath = '/favoritos';
const visitsPath = '/visitas';
const applicationsPath = '/aplicaciones';

db.connect();
var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use(findAppBySecret);
app.use(findAppByApplicationId);
//app.use(authApp.unless({method: 'OPTIONS'}));

app.use(allowCORs.unless({path: '/public'}));

app.use(
  jwtMiddleware({secret: secrets.jwtSecret})
    .unless({path:[sessionsPath,usersPath], method: ['GET','OPTIONS']})
)

app.use(localesPath,locales);
app.use(localesPath,favoritePlaces);
app.use(localesPath,visitsPlaces);
app.use(usersPath,users);
app.use(sessionsPath,sessions);
app.use(favoritesPath,favorites);
app.use(visitsPath,visits);
app.use(applicationsPath,applications);

// const Application = require('./models/Application');

// app.get('/demo',function(req,res){
//   Application.remove({}).then(()=>res.json({}));
// })

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.log(res.locals.error);
  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

module.exports = app;
