const mongoose = require('mongoose');

const dbName = 'places_api';

module.exports = {
    connect: ()=> {mongoose.connect('mongodb://localhost/'+ dbName, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
                            .then(() => console.log('DB Connected!'))
                            .catch(err => {
                            console.log('No se pudo conectar a la DB' + err);
                            });
    },
    dbName,
    connection: ()=>{
        if(mongoose.connection)
            return mongoose.connection;

        return this.connect();
    }
}