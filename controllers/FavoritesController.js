const buildParams = require('./helpers').paramsBuilder;

const validParams = ['_place'];

const FavoritePlace = require('../models/FavoritePlace');

function find(req,res,next){
    FavoritePlace.findById(req.params.id).then(fav=>{
        req.mainObj = fav;
        req.favorite = fav;
        next();
    }).catch(next);
}

function index(req,res){
    let promise = null;

    if(req.place){
        promise = req.place.favorites;
    }else if(req.user){
        promise = FavoritePlace.forUser(req.user.id, req.query.page || 1);
    }

    if(promise){
        promise.then(favorites=>{
            res.json(favorites);
        }).catch(error=>{
            res.status(500).json({error});
        })
    }else{
        res.status(404).json({});
    }
}

function create(req,res){
    let params = buildParams(validParams,req.body);
    params['_user'] = req.user.id;

    FavoritePlace.create(params)
        .then(favorite=>{
            res.json(favorite);
        }).catch(error=>{
            res.status(422).json({error});
        })
}

function destroy(req,res){
    req.favorite.remove().then(doc=>{
        res.json({})
    }).catch(error=>{
        res.status(500).json({error});
    })
}

module.exports = { index, create, find, destroy }