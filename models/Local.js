const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const uploader = require('./Uploader');
const slugify = require('../plugins/slugify');

const FavoritePlace = require('./FavoritePlace');
const Visit = require('./Visit');

let placeSchema = new mongoose.Schema({
    title:{
        type: String,
        required: true
    },
    slug:{
        type: String,
        unique: true
    },
    address: String,
    description: String,
    acceptsCreditCard: {
        type: Boolean,
        default: false
    },
    coverImage: String,
    avatarImage: String,
    openHour: Number,
    closeHour: Number,
    _user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }
});

placeSchema.methods.updateImage = function(path,imageType){
    // Primero subir la imagen
    // Guardar el lugar
    return uploader(path).then(secure_url => this.saveImagerUrl(secure_url,imageType));
}

placeSchema.methods.saveImagerUrl = function(secure_url, imageType){
    this[imageType+'Image'] = secure_url;
    return this.save();
}

placeSchema.pre('save',function(next){
    Place.findById(this.id).then(doc=>{
        if(this.slug && doc.title == this.title) {
            return next();
        }
        generateSlugAndContinue.call(this,0,next);
    }).catch(err=>console.log(err));
});

placeSchema.statics.validateSlugCount = function(slug){
    return Place.countDocuments({slug}).then(count=>{
        if(count > 0) return false;
        return true;
    })
}

placeSchema.plugin(mongoosePaginate);

function generateSlugAndContinue(count,next){
    this.slug = slugify(this.title);
    
    if(count != 0)
    this.slug += "-" + count;

    Place.validateSlugCount(this.slug).then(isValid=>{
        if(!isValid) 
            return generateSlugAndContinue.call(this,count+1,next);

            next();
    })
}

placeSchema.virtual('favorites').get(function(){
    return FavoritePlace.find({'_place': this._id}).sort('-id');
})

placeSchema.virtual('visits').get(function(){
    return Visit.find({'_place': this._id}).sort('-id');
})

const Place = mongoose.model('Place', placeSchema);

module.exports = Place;