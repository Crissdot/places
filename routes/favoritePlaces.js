const express = require('express');
let router = express.Router();

const authenticateOwner = require('../middleware/authenticateOwner');
const favoritesController = require('../controllers/FavoritesController');
const placesController = require('../controllers/LocalesController');

router.route('/:id/favoritos')
    .get(placesController.find,favoritesController.index)
    .post(placesController.find,favoritesController.create);

router.route('/:id/favoritos/:fav_id')
    .delete(favoritesController.find,authenticateOwner,favoritesController.destroy);

module.exports = router;