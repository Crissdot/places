const express = require('express');
let router = express.Router();

const favoritesController = require('../controllers/FavoritesController');

const jwtMiddleware = require('express-jwt');
const secrets = require('../config/secrets');

router.route('/')
    .get(jwtMiddleware({secret: secrets.jwtSecret}),favoritesController.index)
    .post(favoritesController.create);

module.exports = router;