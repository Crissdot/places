const express = require('express');

let router = express.Router();

const localesController = require('../controllers/LocalesController');

const authenticateOwner = require('../middleware/authenticateOwner');

router.route('/')
    .get(localesController.index)
    .post(
        localesController.multerMiddleware(),
        localesController.create,
        localesController.saveImage);

router.route('/:id')
    .get(localesController.find, localesController.show)
    .put(localesController.find, authenticateOwner,localesController.update)
    .delete(localesController.find, authenticateOwner,localesController.destroy);

module.exports = router;