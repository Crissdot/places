const express = require('express');
let router = express.Router();

const visitsController = require('../controllers/VisitsController');

const jwtMiddleware = require('express-jwt');
const secrets = require('../config/secrets');

router.route('/')
    .get(jwtMiddleware({secret: secrets.jwtSecret}), visitsController.index)
    .post(visitsController.create);

module.exports = router;