const express = require('express');
let router = express.Router();

const authenticateOwner = require('../middleware/authenticateOwner');
const visitsController = require('../controllers/VisitsController');
const placesController = require('../controllers/LocalesController');

router.route('/:id/visitas')
    .get(placesController.find,visitsController.index)
    .post(placesController.find,visitsController.create);

router.route('/:id/visitas/:visit_id')
    .delete(visitsController.find,authenticateOwner,visitsController.destroy);

module.exports = router;